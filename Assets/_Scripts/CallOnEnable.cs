﻿using UnityEngine;
using System.Collections;
using UnityEngine.Events;

public class CallOnEnable : MonoBehaviour {
    public UnityEvent onEnableEvent = new UnityEvent();

    private void OnEnable()
    {
        if (onEnableEvent != null) onEnableEvent.Invoke();

    }
}
