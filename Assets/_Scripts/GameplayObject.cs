﻿using System;
using UnityEngine;

[Serializable]
public class GameplayObject
{
/*    [NonSerialized]
    public GameObject gameObject;*/
    [NonSerialized]
    public PoolHelper poolHelper;

    public string prefabName;

    public Vector3 currPos;
    public Quaternion currRot;
    public Vector3 currScale = Vector3.one;


    public void UpdateData()
    {
        if(poolHelper==null || poolHelper.gameObject==null) return;

        prefabName = poolHelper.gameObject.name;

        //transform properties
        currPos = poolHelper.transform.position;
        currRot = poolHelper.transform.rotation;
        currScale = poolHelper.transform.localScale;
    }
}
