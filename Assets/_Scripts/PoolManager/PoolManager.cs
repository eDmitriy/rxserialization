﻿using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UniRx;

public class PoolManager : MonoBehaviour {

    #region Variables

    public static PoolManager instance;

    private List<PoolHelper> pool = new List<PoolHelper>();
    private Vector3 _defaultPosition = Vector3.down*-9000f;

    private Transform cachedTransform;

    [Serializable]
    public struct PoolMember
    {
        public GameObject gameObject;
        public int count;
    }
    public PoolMember[] PoolMembers = new PoolMember[0];
    #endregion


    private void Awake()
    {
        //Init
        instance = this;
        cachedTransform = transform;

        pool.Clear();
        foreach (PoolMember member in PoolMembers)
        {
            AddObject(member.gameObject, member.count);
        }
    }



    public void AddObject(GameObject gameObjectPrefab, int count)
    {
        for (int i = 0; i < count; i++)
        {
            GameObject go = Instantiate(gameObjectPrefab, _defaultPosition, Quaternion.identity) as GameObject;
            if (go != null)
            {
                go.name = gameObjectPrefab.name;
                var ph = go.AddComponent<PoolHelper>();
                pool.Add( ph );

                ReturnToPool( ph );
            }
        }
    }


    public void InstantiatePoolObject(string name, Vector3 pos, Quaternion rot, Func<GameObject, GameObject> returnFunc)
    {
        //GameObject go = null;

        //start func in new thread
        var observable = Observable.Start(
            () => GetPoolHelper(name)
            );
        //wait for thread and get result in main thread
        Observable.WhenAll(observable)
            .ObserveOnMainThread()
            .Subscribe(result =>
            {
                if (result[0] == null)
                {
                    //print("PoolManager instPoolObj result = null" + name);

                    AddObject( PoolMembers.FirstOrDefault(v=>v.gameObject.name==name).gameObject, 1 );
                    result[0] = GetPoolHelper(name);
                    //InstantiatePoolObject(name, pos, rot, returnFunc);
                    //return;
                }
                if(returnFunc!=null && result[0]!=null) returnFunc(InstantiateObj(result[0], pos, rot));
            }
        );
        //return go;
    }

    private PoolHelper GetPoolHelper(string name)
    {
        PoolHelper helper = null;

        helper = pool.FirstOrDefault(v => v.CurrPoolHelperState == PoolHelper.PoolHelperState.Pooled && v.CurrNme == name);
        if (helper != null)
        {
            helper.CurrPoolHelperState = PoolHelper.PoolHelperState.Active;
        }
        return helper;
    }

    private GameObject InstantiateObj(PoolHelper helper, Vector3 pos, Quaternion rot)
    {
        GameObject go = null;
        if (helper != null)
        {
            go = helper.CachedGameObject;
            Rigidbody rb = helper.CachedRigidbody;
            //helper.CurrPoolHelperState = PoolHelper.PoolHelperState.Active;
            SwitchComponents(helper, true);

            if (go != null)
            {
                go.transform.parent = null;
                go.transform.localScale = Vector3.one;
                go.transform.position = pos;
                go.transform.rotation = rot;
            }

            if (rb != null)
            {
                rb.position = pos;
                rb.rotation = rot;

                //rb.velocity = Vector3.zero;
                rb.angularVelocity = Vector3.zero;
            }
        }
        else
        {
            Debug.Log("PoolManager InstantiateObj helper==null");
        }
        return go;
    }


    public void ReturnToPool(PoolHelper poolHelper)
    {
        if (poolHelper == null || poolHelper.CachedGameObject == null)
        {
            return;
        }

        poolHelper.CurrPoolHelperState = PoolHelper.PoolHelperState.Pooled;
        SwitchComponents(poolHelper, false);


        poolHelper.CachedGameObject.transform.parent = cachedTransform;
        poolHelper.CachedGameObject.transform.localScale = Vector3.one;
        poolHelper.CachedGameObject.transform.position = _defaultPosition;

    }

    static void SwitchComponents(PoolHelper helper, bool val)
    {
        if(helper==null) return;

        foreach (MonoBehaviour monoBehaviour in helper.Components)
        {
            monoBehaviour.enabled = val;
        }


        foreach (Collider coll in helper.gameObject.GetComponentsInChildren<Collider>())
        {
            coll.enabled = val;
        }

        if (helper.CachedRigidbody != null && !val)
        {
            helper.CachedRigidbody.velocity = Vector3.zero;
            helper.CachedRigidbody.angularVelocity = Vector3.zero;
        }
    }

    
}