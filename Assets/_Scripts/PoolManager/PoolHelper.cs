﻿using UnityEngine;


public class PoolHelper : MonoBehaviour {

    public enum PoolHelperState {Pooled, Active }
    public PoolHelperState CurrPoolHelperState { get; set; }


    public GameObject CachedGameObject
    {
        get
        {
            if (cachedGameObject == null)
            {
                cachedGameObject = gameObject;
            }
            return cachedGameObject;
        }
    }
    private GameObject cachedGameObject;


    public MonoBehaviour[] Components
    {
        get
        {
            if (components.Length == 0)
            {
                components = GetComponents<MonoBehaviour>();
            }

            return components;
        }
    }
    private MonoBehaviour[] components = new MonoBehaviour[0];


    public Rigidbody CachedRigidbody
    {
        get
        {
            if (cachedRigidbody == null)
            {
                cachedRigidbody = gameObject.GetComponent<Rigidbody>();
            }

            return cachedRigidbody;
        }
    }
    private Rigidbody cachedRigidbody;

    public string CurrNme
    {
        get
        {
            if (currNme.Length < 1) currNme = name;
            return currNme;
        }

    }
    private string currNme="";


    private void Awake()
    {
        currNme = name;
        cachedRigidbody = GetComponent<Rigidbody>();
        cachedGameObject = gameObject;
    }
}
