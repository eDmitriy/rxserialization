﻿using System;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class GameData
{
    public GameplayObject[] GameplayObjects;  // { get; set; }

    public int currScore = 0;
    public int currHealth = 0;

    public int currDifficulty = 1;

    /*public bool isGameOver = false;
    public bool isRestart = false;*/

    public int currGameState;

    public GameData(GameplayObject[] gameplayObjectsArr, int score, int health, int difficult, int gameState)
    {
        GameplayObjects = gameplayObjectsArr;
        foreach (GameplayObject o in GameplayObjects)
        {
            o.UpdateData();
        }


        currScore = score;
        currHealth = health;

        currDifficulty = difficult;

        currGameState = gameState;

        /*isRestart = isRestartEnabled;
        isGameOver = isGameoverNow;*/
    }


}