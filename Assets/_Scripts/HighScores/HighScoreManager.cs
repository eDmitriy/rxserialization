﻿using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using UniRx;
using UnityEngine.UI;

public class HighScoreManager : MonoBehaviour
{
    public string savePath = "";
    public int maxScoresCount = 10;

    public Text highScoreText;

    private Done_GameController gameController;
    HighScoreData data = new HighScoreData();
    
	void Start ()
	{
	    savePath = Application.dataPath + "/" + "Highscores";

	    gameController = GetComponent<Done_GameController>();
	    gameController.currGameState.ObserveEveryValueChanged(v => v.Value).Subscribe(v =>
	    {
	        if (v == GameState.GameOver)
	        {
	            bool isHigh = CheckIsHigh(gameController.score.Value);

                highScoreText.text = (isHigh ? "New high score = "+ lastRecord +" !!!\n":"") + HighscoresDataToString();

                SaveHighScore();
            }
        }).AddTo(this);

	    LoadHighScore();
        highScoreText.text = HighscoresDataToString();
    }


    private int lastRecord = 0;
    private bool CheckIsHigh(int val)
    {
        Sort();
        //int count = data.values.Count(v => v < val);

        if ( data.values.Count == 0 || data.values[0] < val )
        {
            if(data.values.Count == maxScoresCount) data.values.Remove(data.values.Last());

            data.values.Add(val);
            lastRecord = val;
            return true;
        }
        return false;
    }

    void SaveHighScore()
    {
        Sort();

        string jsonData = JsonUtility.ToJson(data, true);

        if (!File.Exists(savePath)) File.Create(savePath).Close();
        File.WriteAllText(savePath, jsonData, Encoding.ASCII);
    }

    void LoadHighScore()
    {
        if (!File.Exists(savePath)) return;

        string jsonData = File.ReadAllText(savePath, Encoding.ASCII);
        if (!string.IsNullOrEmpty(jsonData))
        {
            data = JsonUtility.FromJson<HighScoreData>(jsonData);
        }
    }

    private string HighscoresDataToString()
    {
        Sort();
        return data.values.Aggregate("HIGHSCORES:\n\n", (current, v) => current + v.ToString()+"\n");
    }

    private void Sort()
    {
        data.values = data.values.OrderByDescending(v => v).ToList();
    }
}

[Serializable]
public class HighScoreData
{
    public List<int> values = new List<int>();


}