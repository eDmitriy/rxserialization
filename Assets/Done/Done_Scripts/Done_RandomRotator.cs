﻿using UnityEngine;
using System.Collections;
using UniRx;

public class Done_RandomRotator : MonoBehaviour 
{
	public float tumble;
	private Rigidbody rigidbody;


	void Start ()
	{
		//GetComponent<Rigidbody>().angularVelocity = Random.insideUnitSphere * tumble;
	   // this.ObserveEveryValueChanged(v => enabled).Subscribe(Switch);
	}


    void Awake()
    {
        rigidbody = GetComponent<Rigidbody>();
    }

    private void OnEnable()
    {
        if (rigidbody != null) rigidbody.angularVelocity = Random.insideUnitSphere * tumble;

    }

    private void OnDisable()
    {
        if (rigidbody != null) rigidbody.angularVelocity = Vector3.zero;
    }

}