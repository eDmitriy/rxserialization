﻿using UnityEngine;
using System.Collections;

public class Done_WeaponController : MonoBehaviour
{
	public GameObject shot;
	public Transform shotSpawn;
	public float fireRate;
	public float delay;
    private float lastFireTime = 0;
    private Done_GameController gameController;


    private void Awake()
    {
        GameObject gameControllerObject = GameObject.FindGameObjectWithTag("GameController");
        if (gameControllerObject != null)
        {
            gameController = gameControllerObject.GetComponent<Done_GameController>();
        }
        if (gameController == null)
        {
            Debug.Log("Cannot find 'GameController' script");
        }
    }
    void Start ()
	{
		InvokeRepeating ("Fire", delay, fireRate);
	}


    void Fire ()
	{
        if(!enabled) return;

        //Instantiate(shot, shotSpawn.position, shotSpawn.rotation);
        //PoolManager.instance.InstantiatePoolObject(shot.name, shotSpawn.position, shotSpawn.rotation, v=>null);
        gameController.AddGameObject(shot.name, shotSpawn.position, shotSpawn.rotation);

        //GetComponent<AudioSource>().Play();
        AudioSource audioSource = GetComponent<AudioSource>();
        audioSource.PlayOneShot(audioSource.clip);
    }
}
