﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class Done_Boundary 
{
	public float xMin, xMax, zMin, zMax;
}

public class Done_PlayerController : MonoBehaviour
{
	public float speed;
	public float tilt;
	public Done_Boundary boundary;

	public GameObject shot;
	public Transform shotSpawn;
	public float fireRate;
	private float nextFire;

    private Rigidbody rb;
    private Done_GameController gameController;


    private void Awake()
    {
        rb = GetComponent<Rigidbody>();

        GameObject gameControllerObject = GameObject.FindGameObjectWithTag("GameController");
        if (gameControllerObject != null)
        {
            gameController = gameControllerObject.GetComponent<Done_GameController>();
        }
        if (gameController == null)
        {
            Debug.Log("Cannot find 'GameController' script");
        }
    }

    void Update ()
	{
		if (Input.GetButton("Fire1") && Time.time > nextFire) 
		{
			nextFire = Time.time + fireRate;
            //Instantiate(shot, shotSpawn.position, shotSpawn.rotation);
            //PoolManager.instance.InstantiatePoolObject(shot.name, shotSpawn.position, shotSpawn.rotation, v=>null);
            gameController.AddGameObject(shot.name, shotSpawn.position, shotSpawn.rotation);
            //GetComponent<AudioSource>().Play ();
            AudioSource audioSource = GetComponent<AudioSource>();
            audioSource.PlayOneShot(audioSource.clip);
        }
	}


    float moveHorizontal;
    float moveVertical;
    Vector3 movement;
    void FixedUpdate ()
	{
	    moveHorizontal = Input.GetAxis ("Horizontal");
        moveVertical = Input.GetAxis ("Vertical");

        movement = new Vector3 (moveHorizontal, 0.0f, moveVertical);
        /*GetComponent<Rigidbody>()*/rb.velocity = movement * speed;
		
		/*GetComponent<Rigidbody>()*/rb.position = new Vector3
		(
			Mathf.Clamp (/*GetComponent<Rigidbody>()*/rb.position.x, boundary.xMin, boundary.xMax), 
			0.0f, 
			Mathf.Clamp (/*GetComponent<Rigidbody>()*/rb.position.z, boundary.zMin, boundary.zMax)
		);
		
		/*GetComponent<Rigidbody>()*/rb.rotation = Quaternion.Euler (0.0f, 0.0f, /*GetComponent<Rigidbody>()*/rb.velocity.x * -tilt);
	}
}
