﻿using UnityEngine;
using System.Collections;
using System.Linq;

public class Done_DestroyByContact : MonoBehaviour
{
	public GameObject explosion;
	public GameObject playerExplosion;
	public int scoreValue;
	private Done_GameController gameController;
    //private PoolHelper poolHelper;

	void Start ()
	{
	    //poolHelper = GetComponent<PoolHelper>();

		GameObject gameControllerObject = GameObject.FindGameObjectWithTag ("GameController");
		if (gameControllerObject != null)
		{
			gameController = gameControllerObject.GetComponent <Done_GameController>();
		}
		if (gameController == null)
		{
			Debug.Log ("Cannot find 'GameController' script");
		}
	}

	void OnTriggerEnter (Collider other)
	{
	    if (gameController == null) return;
	    bool destroyColliderObj = true;

		if (other.tag == "Boundary" || other.tag == "Enemy")
		{
			return;
		}

		if (explosion != null)
		{
            //Instantiate(explosion, transform.position, transform.rotation);
            //PoolManager.instance.InstantiatePoolObject(explosion.name, transform.position, transform.rotation, result => null);
            gameController.AddGameObject(explosion.name, transform.position, transform.rotation);
		}

        if (other.tag == "Player")
		{
            //Instantiate(playerExplosion, other.transform.position, other.transform.rotation);
            //PoolManager.instance.InstantiatePoolObject(playerExplosion.name, other.transform.position, other.transform.rotation, v=>null);
            gameController.AddGameObject(playerExplosion.name, other.transform.position, other.transform.rotation);
            destroyColliderObj = gameController.ChangeHealth(-1);
		}

	    gameController.AddScore(scoreValue);


	    if (destroyColliderObj)
	    {
            gameController.RemoveGameObject(other.gameObject);
	    }
        gameController.RemoveGameObject(gameObject);

    }
}