﻿using UnityEngine;
using System.Collections;
using UniRx;

public class Done_Mover : MonoBehaviour
{
	public float speed;
    private Rigidbody rigidbody;

	void Awake ()
	{
	    rigidbody = GetComponent<Rigidbody>();
        //GetComponent<Rigidbody>().velocity = transform.forward * speed;
        //this.ObserveEveryValueChanged(v => enabled).Subscribe(Switch);

    }

    private void OnEnable()
    {
        if (rigidbody != null) rigidbody.velocity = Vector3.forward * speed;

    }

    private void OnDisable()
    {
        if (rigidbody != null) rigidbody.velocity = Vector3.zero;
    }

/*    void Switch(bool val)
    {
        if (val)
        {
            OnEnable();
        }
        else
        {
            OnDisable();
        }
    }*/
}
