﻿using UnityEngine;
using System.Collections;
using System.Linq;

public class Done_DestroyByTime : MonoBehaviour
{
	public float lifetime;
    private Done_GameController gameController;
/*    private PoolHelper poolHelper;
    public PoolHelper PoolHelper
    {
        get
        {
            if (poolHelper == null)
            {
                poolHelper = GetComponent<PoolHelper>();
            }
            return poolHelper;
        }

    }*/

    void Awake ()
	{
        //Destroy (gameObject, lifetime);
        //StartCoroutine(DestroyWithDelay(lifetime));

        //poolHelper = GetComponent<PoolHelper>();
        GameObject gameControllerObject = GameObject.FindGameObjectWithTag("GameController");
        if (gameControllerObject != null)
        {
            gameController = gameControllerObject.GetComponent<Done_GameController>();
        }
        if (gameController == null)
        {
            Debug.Log("Cannot find 'GameController' script");
        }
    }

    private void OnEnable()
    {
        StartCoroutine(DestroyWithDelay(lifetime));
    }


    private IEnumerator DestroyWithDelay(float delay)
    {
        yield return new WaitForSeconds(delay);

        //PoolManager.instance.ReturnToPool(transform.root.GetComponentInChildren<PoolHelper>() );
        gameController.RemoveGameObject(gameObject);

    }
}
