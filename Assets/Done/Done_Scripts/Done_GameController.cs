﻿using System;
using UnityEngine;
using System.Collections;
using System.IO;
using System.Linq;
using System.Text;
using UniRx;
using UnityEngine.UI;
using Random = UnityEngine.Random;


public class Done_GameController : MonoBehaviour
{
    #region Variables

    public ReactiveProperty<GameState> currGameState = new ReactiveProperty<GameState>();

    [Header("Player")]
    public GameObject playerPrefab;
    public Transform playerSpawnPoint;
    public int playerHealth_Max = 3;
    private ReactiveProperty<int> playerHealth = new ReactiveProperty<int>();

    [Header("Hazards")]
    public GameObject[] hazards;
    public Vector3 spawnValues;
    public int hazardCount_Max = 10;
    public int hazardCount;

    [Header("Difficulty")]
    public float difficulty_increaseWaitTime = 3;
    public float difficulty_Max = 10;
    public float difficulty_StartValue = 1;
    private ReactiveProperty<float> difficulty = new ReactiveProperty<float>();

    [Header("Waves")]
    public float spawnWait;
    public float spawnWait_Max = 0.75f;

    public float startWait;
    public float waveWait_Max;
    private float waveWait_Current = 1;

    /*public float waveWait_Min;
    public float waveWait_Step;*/


    [Header("GUI")]
    public Text scoreText;
    public Text playerHealthText;
    //public Text restartText;
    //public Text gameOverText;

    public Transform pauseScreen;

    public Button startGameButton;


    public ReactiveProperty<int> score = new ReactiveProperty<int>();
    public ReactiveCollection<GameplayObject> gamePlayObjects = new ReactiveCollection<GameplayObject>();

    private string saveFilePath;

    #endregion

    
    #region Init

    private void Start()
    {
        saveFilePath = Application.dataPath
            //Application.persistentDataPath 
                       + "/" + "SaveFile";
        Observable.Interval(new TimeSpan(0, 0, 0, (int)difficulty_increaseWaitTime)).Subscribe(x => { ChangeDifficulty(1); }).AddTo(this);

        if (scoreText != null) score.SubscribeToText(scoreText, x => "Score = " + x).AddTo(this);
        if (playerHealthText != null) playerHealth.SubscribeToText(playerHealthText, x => "HP = " + x).AddTo(this);
        

        //Pause screen
        currGameState.ObserveEveryValueChanged(v => v.Value).Subscribe(v =>
        {
            bool isPause = v == GameState.Paused || v == GameState.GameOver;
            Time.timeScale = isPause ? 0 : 1;
            pauseScreen.gameObject.SetActive(isPause);

            startGameButton.gameObject.SetActive( v == GameState.GameOver);
            //if (isPause) scoreBoardText.text = "Records:\n";
        }
        ).AddTo(this);



        RxCollection();
        StartGame();

        StartCoroutine(SpawnWaves());
    }

    public void StartGame()
    {
        /*gameOver = false;
        restart = false;*/
        /*restartText.text = "";
        gameOverText.text = "";*/
        pauseScreen.gameObject.SetActive(false);
        currGameState.Value = GameState.Playing;

        score.Value = 0;
        playerHealth.Value = playerHealth_Max;
        waveWait_Current = waveWait_Max;
        hazardCount = hazardCount_Max;

        //difficulty.Value = difficulty_StartValue;
        ChangeDifficulty(difficulty_StartValue, false);

        //clear gamedata
        while (gamePlayObjects.Count > 0)
        {
            gamePlayObjects.RemoveAt(0);
        }

        //Init player obj
        gamePlayObjects.Add(new GameplayObject()
        {
            prefabName = playerPrefab.name,
            currPos = playerSpawnPoint.position,
            currRot = playerSpawnPoint.rotation
        });

    }

    #endregion


    #region Input

    private void Update()
    {
        if (currGameState.Value == GameState.GameOver)
        {
            if (Input.GetKeyDown(KeyCode.R))
            {
                Application.LoadLevel(Application.loadedLevel);
            }
        }

        //if (currGameState.Value == GameState.Playing){
            if (Input.GetKeyDown(KeyCode.Q))
            {
                SaveData();

            }
            if (Input.GetKeyDown(KeyCode.E))
            {
                LoadData();
            }
        //}

        if (Input.GetKeyDown(KeyCode.P))
        {
            PauseGame();
        }

    }

    #endregion

    #region Waves

    private IEnumerator SpawnWaves()
    {
        yield return new WaitForSeconds(startWait);
        while (true)
        {
            if (currGameState.Value == GameState.Playing)
            {
                for (int i = 0; i < hazardCount; i++)
                {
                    GameObject hazard = hazards[Random.Range(0, hazards.Length)];
                    Vector3 spawnPosition = new Vector3(Random.Range(-spawnValues.x, spawnValues.x), spawnValues.y,
                        spawnValues.z);
                    Quaternion spawnRotation = Quaternion.identity;

                    AddGameObject(hazard.name, spawnPosition, spawnRotation);

                    yield return new WaitForSeconds(spawnWait);
                }
                yield return new WaitForSeconds(waveWait_Current);
            }


/*            if (currGameState.Value == GameState.GameOver)
            {
                restartText.text = "Press 'R' for Restart";
                currGameState.Value = GameState.GameOver;
                //restart = true;
                //break;
            }*/
        }
    }

    #endregion


    #region GameplayObjects actions
    private void RxCollection()
    {
        //Rx will spawn gameobject from pool If new gameplay instance added
        //Pool manager will call async instantiating
        gamePlayObjects.ObserveAdd().Subscribe(x =>
        {
            PoolManager.instance.InstantiatePoolObject(x.Value.prefabName, x.Value.currPos, x.Value.currRot, result =>
            {
                return (x.Value.poolHelper = result.GetComponent<PoolHelper>()).gameObject;
            });
        }
        ).AddTo(this);

        //Rx will return gameobject to pool If gameplay instance removed
        gamePlayObjects.ObserveRemove().Subscribe(x =>
        {
            PoolManager.instance.ReturnToPool(x.Value.poolHelper);
        }
        ).AddTo(this);
    }

    public void AddGameObject(string prefabName, Vector3 spawnPos, Quaternion spawnRot)
    {
        //Instantiate (hazard, spawnPosition, spawnRotation);

        //gamePlayObjects.Add( PoolManager.InstantiatePoolObject(hazard.name, spawnPosition, spawnRotation) );
        gamePlayObjects.Add(new GameplayObject()
        {
            prefabName = prefabName,
            currPos = spawnPos,
            currRot = spawnRot

        });
        //print(hazard.name+" spawned");
    }

    public void RemoveGameObject(GameObject go)
    {
        if (go == null) return;

        GameplayObject gpO = gamePlayObjects.FirstOrDefault(v => v.poolHelper != null && v.poolHelper.gameObject == go);
        if (gpO != null) gamePlayObjects.Remove(gpO);
    }

    #endregion


    #region GameState controllers

    public void AddScore(int newScoreValue)
    {
        score.Value += newScoreValue;
    }

    public bool ChangeHealth(int val, bool additve = true)
    {
        if (additve)
        {
            playerHealth.Value += val;
        }
        else
        {
            playerHealth.Value = val;
        }

        return CheckGameOver();
    }

    private bool CheckGameOver()
    {
        bool gameOver = false;

        if (playerHealth.Value == 0)
        {
            //gameOverText.text = "Game Over!";
            gameOver = true;
            currGameState.Value = GameState.GameOver;
        }
        else
        {
            //gameOverText.text = "";
            currGameState.Value = GameState.Playing;
        }
        return gameOver;
    }

    private void ChangeDifficulty(float val, bool additve=true)
    {
        if (additve)
        {
            difficulty.Value += val;
        }
        else
        {
            difficulty.Value = val;
        }
        difficulty.Value = Mathf.Clamp(difficulty.Value, difficulty_StartValue, difficulty_Max-1);

        float difficultyTemp = (difficulty_Max - difficulty.Value)/difficulty_Max;

        waveWait_Current = waveWait_Max * difficultyTemp;
        spawnWait = spawnWait_Max * difficultyTemp;

        hazardCount = hazardCount_Max * (int)difficulty.Value;

    }

    //GameState gameStateBeforePause = GameState.Playing;
    public void PauseGame()
    {

        currGameState.Value = currGameState.Value == GameState.Paused ? GameState.Playing : GameState.Paused;
    }

    #endregion


    #region Save&Load

    public void SaveData()
    {
        //GameObject[] gosToSerialize = gamePlayObjects.Select(v => v.poolHelper.gameObject).ToArray();
        GameData gameData = new GameData(
            /*gosToSerialize*/
            gamePlayObjects.Where(v=>v.poolHelper!=null).ToArray(), 
            score.Value, 
            playerHealth.Value, 
            (int)difficulty.Value, 
            (int)currGameState.Value
            );
        string jsonData = JsonUtility.ToJson(gameData, true);

        if (!File.Exists(saveFilePath)) File.Create(saveFilePath).Close();
        File.WriteAllText(saveFilePath, jsonData, Encoding.ASCII);

        //print(jsonData);
    }



    private bool loadingDataNow = false;

    public void LoadData()
    {
        if (loadingDataNow) return;
        if (!File.Exists(saveFilePath)) return;

        loadingDataNow = true;
        //Start new thread for async load
        var observable = Observable.Start(
            () =>
            {
                GameData gameData = null;
                string jsonData = File.ReadAllText(saveFilePath, Encoding.ASCII);
                if (!string.IsNullOrEmpty(jsonData))
                {
                    gameData = JsonUtility.FromJson<GameData>(jsonData);
                }
                return gameData;
                //print("AsyncLoad threadID = " + Thread.CurrentThread.ManagedThreadId);
            }

            );
        //catch result of async load thread in main thread
        Observable.WhenAll(observable)
            .ObserveOnMainThread()
            .Subscribe(result =>
            {
                LoadDataASync_Callback(result[0]);
                //print("AsyncLoad Result threadID = " + Thread.CurrentThread.ManagedThreadId);
            }
            ).AddTo(this);

    }

    private void LoadDataASync_Callback(GameData gameData)
    {
        if (gameData == null) return;

        while (gamePlayObjects.Count > 0)
        {
            gamePlayObjects.RemoveAt(0);
        }

        for (int i = 0; i < gameData.GameplayObjects.Length; i++)
        {
            gamePlayObjects.Add(gameData.GameplayObjects[i]);
        }
        //print(gamePlayObjects.Count);

        score.Value = gameData.currScore;
        ChangeHealth(gameData.currHealth, false);
        ChangeDifficulty(gameData.currDifficulty,false);

        /*restart = gameData.isRestart;
        gameOver = gameData.isGameOver;*/

        currGameState.Value = (GameState)gameData.currGameState;


        loadingDataNow = false;

    }

    #endregion


}

public enum GameState 
{
    WaitingForStart = 0,
    Playing = 1,
    Paused = 2 ,
    GameOver = 3
}
